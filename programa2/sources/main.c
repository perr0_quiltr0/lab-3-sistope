#include <time.h>
#include <llenarArreglo.h>

#define LARGO 536870912

int main()
{
	srand(time(NULL));

	//reservando memoria para el arreglo
	printf("reservando memoria para el arreglo \n");
	int *ARRAY = malloc(sizeof(int) * LARGO);

	//preparando contadores de tiempo
	clock_t tInicio , tFinal ;

	//llenando arreglo
	printf("llenando el arreglo \n");

		tInicio = clock() ; //capturando tiempo de inicio
		llenarArreglo(ARRAY,LARGO); //llenando arreglo
		tFinal = clock() ; //capturando tiempo de final

	//calculando tiempo total


	//liberando memoria
	free(ARRAY);
	ARRAY = NULL;

	
	//fin de ejecución
	printf("Listo!\n");
	printf("Tiempo transcurrido en llenar arreglo : %f segundos\n", (tFinal-tInicio)/(double)CLOCKS_PER_SEC);

	return EXIT_SUCCESS;
}
